package com.example.demo;

import java.sql.*;

public class Main {


    private static final String MYSQL_DRIVER = "org.h2.Driver";
    private static final String MYSQL_DB_URL = "jdbc:mysql://localhost:3306/-----> DB NAME<-----"; // To dajte meno BD
    private static final String MYSQL_DB_USER = "-----> MYSQL WORKBENCH USERNAME<-----";// To dajte username do mysql workbench je to casto root
    private static final String MYSQL_DB_PASSWORD = "MYSQL WORKBENCH PASS";// // To dajte password do mysql workbench je to casto root

    public static void main(String[] args) {
        try {
            useMySql();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }


    private static void useMySql() throws ClassNotFoundException {
        useDatabase(MYSQL_DRIVER, MYSQL_DB_URL, MYSQL_DB_USER, MYSQL_DB_PASSWORD);
    }

    private static void useDatabase(String driver, String url, String user, String password) throws ClassNotFoundException {
        Class.forName(driver);

        try (Connection connection = DriverManager.getConnection(url, user, password)) {

            drop(connection);
            create(connection);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static void drop(Connection connection) {
        try (Statement dropStatement = connection.createStatement()) {
            dropStatement.execute("DROP TABLE IF EXISTS MOVIES");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static void create(Connection connection) {
        try (Statement createTableStatement = connection.createStatement()) {
            String createTableQuery = "CREATE TABLE IF NOT EXISTS MOVIES (" +
                    "id INTEGER AUTO_INCREMENT, " +
                    "title VARCHAR(255), " +
                    "genre VARCHAR(255), " +
                    "yearOfRelease INTEGER, " +
                    "PRIMARY KEY (id))";
            createTableStatement.execute(createTableQuery);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
